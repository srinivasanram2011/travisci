# TravisCI


**About Framework****
* This framworkwork covers cucumber BDD ,jUnit ,TestNg ,Maven and Selenium with Java.
* pageObjectModule is created to resuse the functionality
* Utility file is created to reuse the methods 
* pageFactory to handle elements and resue it wherever required within the functional module
* Base class is the main class which takes care of Browser setup, loading configuration file and other reusable methods like screenshot, handling sync issues and many more.
  With base class you can avoid code duplication and can reuse the code as much you want.
* Data drivern and Modular drivern framework

**Feature File:**
A Feature File is an entry point to the Cucumber tests.
This is a file where you will describe your tests in Descriptive language (Like English).
It is an essential part of Cucumber, as it serves as an automation test script as well as live documents. 
A feature file can contain a scenario or can contain many scenarios in a single feature file but it usually contains a list of scenarios.

**Step Definition file:**
A Step Definition is a small piece of code with a pattern attached to it or in other words a Step Definition is a java method in a class with an annotation above it.
An annotation followed by the pattern is used to link the Step Definition to all the matching Steps, and the code is what Cucumber will execute when it sees a Gherkin Step.
Cucumber finds the Step Definition file with the help of the Glue code in Cucumber Options.

**Runner class:**
Cucumber uses Junit framework to run.As Cucumber uses Junit we need to have a Test Runner class. This class will use the Junit annotation @RunWith(), which tells JUnit what is the test runner class.
Its more like a starting point for Junit to start executing your tests. I have created my runner class here --> \src\test\java\Runners.java

**Test Report:**
Cucumber has inbuild report with different extensions like json,xml,html etc .for this project it is generated under --> TravisCIAutomationTask\target







