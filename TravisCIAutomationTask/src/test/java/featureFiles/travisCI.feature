Feature: Verify the functionality of TravisCI

  Scenario: Integrate your repository to TravisCI
    Given enter the URL into the browser
    When click sign in with Github
    Then Add repository
    Then verify activate enabled

  Scenario: Verify the status of the TravisCI
    When click travisCI status link
    And get the update to status

  Scenario: Verify the the customize environment variable is added
    And come back to mainWindow
    Then Go to settings and select Assignment
    Then add environment variables





