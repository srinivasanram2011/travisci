import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        plugin = {"pretty","html:target/cucumber_1"},
        tags = {"not @Ignore"},
        features = "src/test/java/featureFiles/travisCI.feature",
        glue = {"step_definitions"},
        junit = {"--step-notifications"})


public class Runners {
}
