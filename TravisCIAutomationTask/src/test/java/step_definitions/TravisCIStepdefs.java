package step_definitions;

import cucumber.api.java8.En;
import pageObjectModule.HomePage;
import pageObjectModule.MyRepositoryPage;
import pageObjectModule.ReuseUtils;
import pageObjectModule.StatusPage;



import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class TravisCIStepdefs  implements En  {
    public TravisCIStepdefs() throws IOException {
        String URL;
        String userName;
        String password;
        String setEnvId;
        String setEnvVal;
        String projectName;
        HomePage homePageObj=new HomePage();
        StatusPage statusDisplayPage=new StatusPage();
        ReuseUtils utilityObj=new ReuseUtils();
        MyRepositoryPage myRepoObj=new MyRepositoryPage();


            FileReader reader=new FileReader("src/main/java/retrieveData/property.properties");
            Properties read=new Properties();
            read.load(reader);
            URL=read.getProperty("url");
            userName=read.getProperty("userName");
            password=read.getProperty("password");
            setEnvId=read.getProperty("setEnvId");
            setEnvVal=read.getProperty("setEnvVal");
            projectName=read.getProperty("projectName");

        Given("^enter the URL into the browser$", () -> {
            homePageObj.launchApplication(URL,"CHROME");
        });
        When( "^click sign in with Github$", () -> {
            homePageObj.signIn(userName,password);
        } );
        Then( "^Add repository$", () -> {
            homePageObj.addRespositiory();
        } );
        Then( "^verify activate enabled$", () -> {
            homePageObj.activateButtonEnabled();
        } );

        When("^click travisCI status link$", () -> {
            utilityObj.scrollTheWindow( "down" );
            homePageObj.clickTravisCIStatusLink();
        });
        And("^get the update to status$", () -> {
            utilityObj.windowHandle();
            statusDisplayPage.getStatus();
            //statusDisplayPage.verifyTheStatus();
        });

        And( "^come back to mainWindow$", () -> {
            utilityObj.backToParentWindow();
            utilityObj.navigate("back");

        } );
        Then( "^Go to settings and select Assignment$", () -> {
            homePageObj.selectRepository(projectName);
        } );
        Then( "^add environment variables$", () -> {
            utilityObj.scrollTheWindow("down");
            myRepoObj.addEnvironmentVariables(setEnvId,setEnvVal);
            utilityObj.tearDown();
        } );


    }
}
