package pageObjectModule;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {

    public static WebDriver driver;
    @BeforeMethod
    public void before(){

    }
    @AfterMethod
    public void afterMethod(){
        driver.quit();
    }
}
