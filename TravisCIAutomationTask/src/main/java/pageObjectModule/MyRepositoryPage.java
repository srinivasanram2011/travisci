package pageObjectModule;

import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MyRepositoryPage extends BaseTest {
    ReuseUtils utilityObj=new ReuseUtils();
    @FindBy(xpath = "//button[@class=\"option-display\"]")
    static WebElement clickMoreButton;
    @FindBy(xpath = "//*[@class=\"option-dropdown\"]//li")
    static List<WebElement> getList;
    @FindBy(css = "#ember108")
    static WebElement selectSettings;
    @FindBy(xpath = "//*[@id=\"ember1523\"]//following::li[@id=\"tab_new\"]")
    static WebElement clickPlusSignToAddRespository;
    @FindBy(xpath = "//*[@class=\"non-admin ember-view profile-repo\"]")
    static List<WebElement> repositories;
    @FindBy(xpath = "//button[@aria-checked=\"false\"]//div")
    static List<WebElement> clickOnToConnect;
    @FindBy(xpath = "//*[@class=\"ember-view button profile-settings\"]")
    static List<WebElement> settingsButton;
    @FindBy(xpath = "//input[@placeholder=\"Name\"]")
    static WebElement name;
    @FindBy(xpath = "//input[@placeholder=\"Value\"]")
    static WebElement value;
    @FindBy(xpath = "//button[@class=\"add-env-form-submit form-submit\"]")
    static WebElement addButton;
    @FindBy(xpath = "//div[@class=\"env-var-name\"]")
    static WebElement newAddedEnvironment;
    @FindBy(xpath = "//*[@class=\"settings-envvar newly-created ember-view\"]//following::button[@class=\"no-button env-var-delete ember-tooltip-target\"]")
    static WebElement deleteIcon;



    public void clickMoreOption(){
        PageFactory.initElements( driver, MyRepositoryPage.class );
        driver.manage().timeouts().implicitlyWait( 30, TimeUnit.SECONDS );
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(1000,0)");
        clickMoreButton.sendKeys( Keys.ENTER );

    }
    public void selectSettings(String options){
    for(int i=0;i<=getList.size()-1;i++){
        if(getList.get( i ).getText().equals( options )){
            //System.out.println("hello list"+ getList.get( i ).getText() );
           // getList.get( i ).sendKeys( Keys.ENTER );
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("window.scrollBy(1000,0)");
            selectSettings.sendKeys( Keys.ENTER );

        }
    }

    }

    public void addEnvironmentVariables(String env_id,String values){
        PageFactory.initElements( driver, MyRepositoryPage.class );
        utilityObj.scrollTheWindow( "down" );
        name.sendKeys(env_id);
        value.sendKeys(values);
        addButton.sendKeys(Keys.ENTER);
        utilityObj.waitForElement("//div[@class=\"env-var-name\"]");
        if(newAddedEnvironment.isDisplayed()){
            Assert.assertEquals(newAddedEnvironment.getText().substring(0,7),env_id);
            deleteIcon.sendKeys(Keys.ENTER);
        }else{
            Assert.fail("new data not added");
        }



    }

}
