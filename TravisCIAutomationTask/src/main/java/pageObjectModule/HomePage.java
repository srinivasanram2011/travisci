package pageObjectModule;

import io.github.bonigarcia.wdm.WebDriverManager;
import net.bytebuddy.asm.Advice;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static io.github.bonigarcia.wdm.DriverManagerType.CHROME;

public class HomePage extends BaseTest{
    ReuseUtils utilityObj=new ReuseUtils();
    @FindBy(xpath = "//*[@class=\"ember-view status-link\"]")
    static WebElement statusLink;
    @FindBy(css = "#profile-menu-link")
    static WebElement clickSignInWithGitHubLink;
    @FindBy(css = "#login_field")
    static WebElement userName;
    @FindBy(css = "#password")
    static WebElement password;
    @FindBy(xpath = "//*[@type=\"submit\"]")
    static WebElement submit;
    @FindBy(css = "#tab_new")
    static WebElement clickPlusSignToAddRespository;
    @FindBy(xpath = "//button[@aria-checked=\"false\"]//div")
    static List<WebElement> clickOnToConnect;
    @FindBy(xpath = "//*[@class=\"non-admin ember-view profile-repo\"]")
    static List<WebElement> repositories;
    @FindBy(xpath = "//*[@class=\"button button--blue\"]")
    static WebElement activateButton;
    @FindBy(xpath = "//*[@class=\"ember-view button profile-settings\"]")
    static List<WebElement> settingsButton;
    @FindBy(css="#tab_current")
    static WebElement currentTab;





    public void initialRequest(String browser)
    {

        switch (browser){
            case "CHROME":
                WebDriverManager.getInstance(CHROME).setup();
                DesiredCapabilities acceptSSlCertificate = DesiredCapabilities.chrome();
                acceptSSlCertificate.setCapability( CapabilityType.ACCEPT_SSL_CERTS, true );
                driver = new ChromeDriver( acceptSSlCertificate );
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );
                break;
            case "FIREFOX":
                WebDriverManager.firefoxdriver().setup();
                DesiredCapabilities acceptSSlCertificates = DesiredCapabilities.firefox();
                acceptSSlCertificates.setCapability( CapabilityType.ACCEPT_SSL_CERTS, true );
                driver = new FirefoxDriver();
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );
                break;

        }
        PageFactory.initElements( driver, HomePage.class );

    }
    public void launchApplication(String url,String browser){
        initialRequest(browser);
        driver.manage().deleteAllCookies();
        driver.get( url );



    }
    public void signIn(String loginid,String pwd){
        clickSignInWithGitHubLink.click();
        driver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );
        userName.sendKeys(loginid);
        password.sendKeys(pwd);
        submit.click();
    }
    public void addRespositiory(){
       clickPlusSignToAddRespository.click();
       driver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );
       //System.out.println( repositories.size() );
       for(int i=0;i<=repositories.size()-1;i++){
           //System.out.println( "print"+i );
           String getRepository=repositories.get( i ).getText();

           if(getRepository.equals( "myAssignment" )){
            //clickOnToConnect.get( i ).click();
             settingsButton.get(i).click();
             currentTab.click();

             this.scrollDown();

           }
       }


    }
    public void selectRepository(String projectName){
        driver.navigate().refresh();
        driver.manage().timeouts().implicitlyWait( 30, TimeUnit.SECONDS );
        WebDriverWait wait = new WebDriverWait(driver, 3);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,-1000)");
        driver.manage().timeouts().implicitlyWait( 30, TimeUnit.SECONDS );
        wait.until( ExpectedConditions.elementToBeClickable( By.cssSelector("#tab_new")));
        driver.navigate().refresh();
        clickPlusSignToAddRespository.click();
        for(int i=0;i<=repositories.size()-1;i++) {
            //System.out.println( "print"+i );
            String getRepository = repositories.get( i ).getText();
            if(getRepository.equals(projectName)){
                //clickOnToConnect.get( i ).click();
                settingsButton.get(i).click();
            }

        }

    }
    public void activateButtonEnabled(){
        if(activateButton.isEnabled()){
            Assert.assertTrue( true );
            Assert.assertTrue("Activate Button should be present",true);
        }else{
            Assert.assertFalse( false );
            Assert.assertFalse( "Activate button should not be displayed",false );


        }
    }

    public void scrollDown(){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,1000)");

    }
    public void clickTravisCIStatusLink(){
        System.out.println("hello "+statusLink.getText());
        statusLink.sendKeys(Keys.ENTER);

    }



}
