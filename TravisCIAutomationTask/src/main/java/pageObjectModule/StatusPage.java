package pageObjectModule;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class StatusPage extends BaseTest{

    @FindBy(xpath = "//*[@class=\"component-container border-color\"]")
    static List<WebElement> status;
    @FindBy(xpath = "//span[@class=\"status font-large\"]")
    static WebElement checkStatus;
    @FindBy(xpath="//*[@class=\"component-status \"]")
    static List<WebElement> componentStatus;
    @FindBy(xpath = "//*[@class=\"component-container border-color is-group\"]")
    static List<WebElement> groupComponents;



public void getStatus(){
    PageFactory.initElements( driver, StatusPage.class );
    checkStatus.isDisplayed();
    String getOverallStatus=checkStatus.getText().trim();
    if(getOverallStatus.equals("All Systems Operational")){
        Assert.assertEquals( getOverallStatus,"All Systems Operational" );
    }else{
        Assert.assertEquals( getOverallStatus,"Partially Degraded Service" );
    }

}
public void verifyTheStatus() {
    driver.manage().timeouts().implicitlyWait( 20, TimeUnit.SECONDS );
    System.out.println( driver.getTitle() );
    int getAllStatus = driver.findElements( By.xpath( "//*[@class=\"component-container border-color\"]" ) ).size();
    for (int i = 0; i <= getAllStatus - 1; i++) {
        status.get( i ).getText();
        System.out.println( status.get( i ).getText() );
        String getcomponentStatus = componentStatus.get( i ).getText();
        System.out.println( i+getcomponentStatus );
        Assert.assertEquals( "Check all component status", getcomponentStatus, "Operational" );

    }
}



}


