package pageObjectModule;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;
import java.util.Set;

public class ReuseUtils extends BaseTest {

    public void windowHandle(){
        Set<String> allWindowHandles=driver.getWindowHandles();
        String lastWindow="";
        for(String handle:allWindowHandles){
            driver.switchTo().window(handle);
            driver.get("https://www.traviscistatus.com/");
            lastWindow=handle;
        }
    }
    public void waitForElement(String path){
        WebDriverWait wait=new WebDriverWait(driver,100);
        wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath(path)) ));
    }
        public void backToParentWindow() {
        String MainWindow = driver.getWindowHandle();
        Set<String> allWindows = driver.getWindowHandles();
        Iterator<String> getwindow = allWindows.iterator();
        while (getwindow.hasNext()) {
            String windows = getwindow.next();
            if (!windows.contains( MainWindow )) {
                driver.switchTo().window( windows );
            }

        }
    }
    public void navigate(String directions){
        if(directions.equals( "back" )){
            driver.navigate().back();
        }else{
            driver.navigate().forward();
        }

    }
    public void scrollTheWindow(String scroll){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        if(scroll.equals( "down" )){
            js.executeScript("window.scrollBy(0,1000)");
        }
        else if(scroll.equals( "up" )){
            js.executeScript("window.scrollBy(0,-1000)");
        }

    }
      public void tearDown(){
        driver.quit();
    }


}
